from django.apps import AppConfig


class GitmatePrSizeLabellerConfig(AppConfig):
    name = 'gitmate_pr_size_labeller'
