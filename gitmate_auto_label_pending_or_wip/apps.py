from django.apps import AppConfig


class GitmateAutoLabelPendingOrWipConfig(AppConfig):
    name = 'gitmate_auto_label_pending_or_wip'
