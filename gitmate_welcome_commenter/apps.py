from django.apps import AppConfig


class GitmateWelcomeCommenterConfig(AppConfig):
    name = 'gitmate_welcome_commenter'
